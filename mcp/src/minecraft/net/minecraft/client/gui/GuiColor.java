package net.minecraft.client.gui;

import net.minecraft.util.EnumChatFormatting;

public class GuiColor {
	
	public static EnumChatFormatting BLACK = EnumChatFormatting.BLACK;
	public static EnumChatFormatting DARK_BLUE = EnumChatFormatting.DARK_BLUE;
	public static EnumChatFormatting DARK_GREEN = EnumChatFormatting.DARK_GREEN;
	public static EnumChatFormatting DARK_AQUA = EnumChatFormatting.DARK_AQUA;
	public static EnumChatFormatting DARK_RED = EnumChatFormatting.DARK_RED;
	public static EnumChatFormatting DARK_PURPLE = EnumChatFormatting.DARK_PURPLE;
	public static EnumChatFormatting GOLD = EnumChatFormatting.GOLD;
	public static EnumChatFormatting GRAY = EnumChatFormatting.GRAY;
	public static EnumChatFormatting DARK_GRAY = EnumChatFormatting.DARK_GRAY;
	public static EnumChatFormatting BLUE = EnumChatFormatting.BLUE;
	public static EnumChatFormatting GREEN = EnumChatFormatting.GREEN;
	public static EnumChatFormatting AQUA = EnumChatFormatting.AQUA;
	public static EnumChatFormatting RED = EnumChatFormatting.RED;
	public static EnumChatFormatting LIGHT_PURPLE = EnumChatFormatting.LIGHT_PURPLE;
	public static EnumChatFormatting YELLOW = EnumChatFormatting.YELLOW;
	public static EnumChatFormatting WHITE = EnumChatFormatting.WHITE;
	public static EnumChatFormatting RESET = EnumChatFormatting.RESET;
	public static EnumChatFormatting ITALIC = EnumChatFormatting.ITALIC;
	public static EnumChatFormatting BOLD = EnumChatFormatting.BOLD;
	public static EnumChatFormatting UNDERLINE = EnumChatFormatting.UNDERLINE;
	public static EnumChatFormatting STRIKETHROUGH = EnumChatFormatting.STRIKETHROUGH;
	
}
