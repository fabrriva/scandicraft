package net.minecraft.client.gui;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.achievement.GuiAchievements;
import net.minecraft.client.gui.achievement.GuiStats;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.resources.I18n;

public class GuiIngameMenu extends GuiScreen
{
    private int field_146445_a;
    private int field_146444_f;
    private static final String __OBFID = "CL_00000703";

    /**
     * Adds the buttons (and other controls) to the screen in question.
     */
    public void initGui()
    {
        this.field_146445_a = 0;
        this.buttonList.clear();
        byte var1 = -16;
        boolean var2 = true;
        this.buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + var1, I18n.format("�bSauvegarder et quitter", new Object[0])));

        if (!this.mc.isIntegratedServerRunning())
        {
            ((GuiButton)this.buttonList.get(0)).displayString = I18n.format("�bD�connecter", new Object[0]);
        }

        this.buttonList.add(new GuiButton(4, this.width / 2 - 22, this.height / 4 + 24 + var1, 122, 20, I18n.format("�bRetourner en jeu", new Object[0])));
        this.buttonList.add(new GuiButton(0, this.width / 2 - 22, this.height / 4 + 96 + var1, 122, 20, I18n.format("�bOptions..", new Object[0])));
        this.buttonList.add(new GuiButton(5, this.width / 2 - 22, this.height / 4 + 72 + var1, 122, 20, I18n.format("�bAchievements", new Object[0])));
        this.buttonList.add(new GuiButton(6, this.width / 2 - 22, this.height / 4 + 48 + var1, 122, 20, I18n.format("�bStats", new Object[0])));
        
        this.buttonList.add(new GuiButton(10, this.width / 2 - 100, this.height / 4 + 24 + var1, 72, 20, I18n.format("�eSite", new Object[0])));
        this.buttonList.add(new GuiButton(11, this.width / 2 - 100, this.height / 4 + 96 + var1, 72, 20, I18n.format("�2Forum", new Object[0])));
        this.buttonList.add(new GuiButton(12, this.width / 2 - 100, this.height / 4 + 72 + var1, 72, 20, I18n.format("�9TeamSpeak", new Object[0])));
        this.buttonList.add(new GuiButton(13, this.width / 2 - 100, this.height / 4 + 48 + var1, 72, 20, I18n.format("�5Boutique", new Object[0])));
    }

    protected void actionPerformed(GuiButton p_146284_1_)
    {
        switch (p_146284_1_.id)
        {
            case 0:
                this.mc.displayGuiScreen(new GuiOptions(this, this.mc.gameSettings));
                break;

            case 1:
                p_146284_1_.enabled = false;
                this.mc.theWorld.sendQuittingDisconnectingPacket();
                this.mc.loadWorld((WorldClient)null);
                this.mc.displayGuiScreen(new GuiMainMenu());

            case 2:
            case 3:
            default:
                break;

            case 4:
                this.mc.displayGuiScreen((GuiScreen)null);
                this.mc.setIngameFocus();
                break;

            case 5:
                this.mc.displayGuiScreen(new GuiAchievements(this, this.mc.thePlayer.func_146107_m()));
                break;

            case 6:
                this.mc.displayGuiScreen(new GuiStats(this, this.mc.thePlayer.func_146107_m()));
                break;
               
            case 10:
            {
            	URI ts = URI.create("http://scandicraft.net/");
            	try{
            		Desktop.getDesktop().browse(ts);
            	} catch(IOException e){
            		e.printStackTrace();
            	}
            }
            break;
            
            case 11:
            {
            	URI ts = URI.create("http://scandicraft.net/");
            	try{
            		Desktop.getDesktop().browse(ts);
            	} catch(IOException e){
            		e.printStackTrace();
            	}
            }         
            break;
                
            case 12:
                {
                	URI ts = URI.create("ts3server://149.202.139.62?port=10203&nickname=" + mc.getSession().getUsername());
                	try{
                		Desktop.getDesktop().browse(ts);
                	} catch(IOException e){
                		e.printStackTrace();
                	}
                }
                break;
            case 13:
            {
            	URI ts = URI.create("http://scandicraft.net/store");
            	try{
            		Desktop.getDesktop().browse(ts);
            	} catch(IOException e){
            		e.printStackTrace();
            	}
            	break;
            }
        }
    }

    /**
     * Called from the main game loop to update the screen.
     */
    public void updateScreen()
    {
        super.updateScreen();
        ++this.field_146444_f;
    }

    /**
     * Draws the screen and all the components in it.
     */
    public void drawScreen(int par1, int par2, float par3)
    {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRendererObj, "�6--[ �4�lScandi�8�lCraft �6]--", this.width / 2, 20, 16777215);
        this.drawCenteredString(this.fontRendererObj, "�3Bienvenue sur ScandiCraft �c" + Minecraft.getMinecraft().getSession().getUsername(), this.width / 2, 35, 16777215);
        super.drawScreen(par1, par2, par3);
    }
}
