package net.minecraft.client;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.IResource;
import net.minecraft.client.resources.IResourceManager;
import net.minecraft.client.resources.IResourceManagerReloadListener;
import net.minecraft.util.ResourceLocation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AntiTransparency
{
    static boolean lostingamefocus = true;
    static List<String> allow = new ArrayList();
    private static final Logger logger = LogManager.getLogger();

    static void init()
    {
        if (allow.size() == 0)
        {
            allow.add("textures/blocks/blockDiamond.png");
            allow.add("textures/blocks/enchantment_bottom.png");
            allow.add("textures/blocks/wood.png");
            allow.add("textures/blocks/blockEmerald.png");
            allow.add("textures/blocks/netherquartz.png");
            allow.add("textures/blocks/snow.png");
            allow.add("textures/blocks/wood_birch.png");
            allow.add("textures/blocks/dispenser_front_vertical.png");
            allow.add("textures/blocks/wood_spruce.png");
            allow.add("textures/blocks/tnt_top.png");
            allow.add("textures/blocks/bed_feet_top.png");
            allow.add("textures/blocks/whiteStone.png");
            allow.add("textures/blocks/quartzblock_top.png");
            allow.add("textures/blocks/blockRedstone.png");
            allow.add("textures/blocks/stonebricksmooth_mossy.png");
            allow.add("textures/blocks/oreDiamond.png");
            allow.add("textures/blocks/sandstone_top.png");
            allow.add("textures/blocks/stonebricksmooth.png");
            allow.add("textures/blocks/gravel.png");
            allow.add("textures/blocks/blockGold.png");
            allow.add("textures/blocks/sand.png");
            allow.add("textures/blocks/quartzblock_bottom.png");
            allow.add("textures/blocks/lightgem.png");
            allow.add("textures/blocks/sandstone_side.png");
            allow.add("textures/blocks/bookshelf.png");
            allow.add("textures/blocks/tree_top.png");
            allow.add("textures/blocks/dispenser_front.png");
            allow.add("textures/blocks/oreGold.png");
            allow.add("textures/blocks/leaves_jungle_opaque.png");
            allow.add("textures/blocks/bed_head_top.png");
            allow.add("textures/blocks/stone.png");
            allow.add("textures/blocks/workbench_top.png");
            allow.add("textures/blocks/oreLapis.png");
            allow.add("textures/blocks/obsidian.png");
            allow.add("textures/blocks/tree_jungle.png");
            allow.add("textures/blocks/furnace_top.png");
            allow.add("textures/blocks/mycel_side.png");
            allow.add("textures/blocks/workbench_front.png");
            allow.add("textures/blocks/jukebox_top.png");
            allow.add("textures/blocks/brewingStand_base.png");
            allow.add("textures/blocks/cloth_7.png");
            allow.add("textures/blocks/cloth_8.png");
            allow.add("textures/blocks/cloth_5.png");
            allow.add("textures/blocks/cloth_6.png");
            allow.add("textures/blocks/stonebrick.png");
            allow.add("textures/blocks/brick.png");
            allow.add("textures/blocks/cloth_9.png");
            allow.add("textures/blocks/sponge.png");
            allow.add("textures/blocks/sandstone_smooth.png");
            allow.add("textures/blocks/cloth_15.png");
            allow.add("textures/blocks/cloth_14.png");
            allow.add("textures/blocks/cloth_11.png");
            allow.add("textures/blocks/cloth_0.png");
            allow.add("textures/blocks/tree_side.png");
            allow.add("textures/blocks/cloth_10.png");
            allow.add("textures/blocks/cloth_13.png");
            allow.add("textures/blocks/cloth_12.png");
            allow.add("textures/blocks/cloth_4.png");
            allow.add("textures/blocks/cloth_3.png");
            allow.add("textures/blocks/stoneMoss.png");
            allow.add("textures/blocks/cloth_2.png");
            allow.add("textures/blocks/cloth_1.png");
            allow.add("textures/blocks/oreCoal.png");
            allow.add("textures/blocks/wood_jungle.png");
            allow.add("textures/blocks/lava_flow.png");
            allow.add("textures/blocks/farmland_wet.png");
            allow.add("textures/blocks/furnace_front.png");
            allow.add("textures/blocks/quartzblock_chiseled.png");
            allow.add("textures/blocks/grass_top.png");
            allow.add("textures/blocks/stonebricksmooth_cracked.png");
            allow.add("textures/blocks/pumpkin_top.png");
            allow.add("textures/blocks/blockIron.png");
            allow.add("textures/blocks/tree_spruce.png");
            allow.add("textures/blocks/grass_side.png");
            allow.add("textures/blocks/redstoneLight.png");
            allow.add("textures/blocks/stoneslab_side.png");
            allow.add("textures/blocks/leaves_spruce_opaque.png");
            allow.add("textures/blocks/leaves_opaque.png");
            allow.add("textures/blocks/quartzblock_side.png");
            allow.add("textures/blocks/workbench_side.png");
            allow.add("textures/blocks/hellrock.png");
            allow.add("textures/blocks/sandstone_bottom.png");
            allow.add("textures/blocks/snow_side.png");
            allow.add("textures/blocks/furnace_side.png");
            allow.add("textures/blocks/bedrock.png");
            allow.add("textures/blocks/tnt_side.png");
            allow.add("textures/blocks/farmland_dry.png");
            allow.add("textures/blocks/quartzblock_chiseled_top.png");
            allow.add("textures/blocks/tree_birch.png");
            allow.add("textures/blocks/furnace_front_lit.png");
            allow.add("textures/blocks/pumpkin_side.png");
            allow.add("textures/blocks/commandBlock.png");
            allow.add("textures/blocks/stoneslab_top.png");
            allow.add("textures/blocks/pumpkin_face.png");
            allow.add("textures/blocks/melon_top.png");
            allow.add("textures/blocks/pumpkin_jack.png");
            allow.add("textures/blocks/dirt.png");
            allow.add("textures/blocks/mycel_top.png");
            allow.add("textures/blocks/melon_side.png");
            allow.add("textures/blocks/blockLapis.png");
            allow.add("textures/blocks/hellsand.png");
            allow.add("textures/blocks/tnt_bottom.png");
            allow.add("textures/blocks/sandstone_carved.png");
            allow.add("textures/blocks/musicBlock.png");
            allow.add("textures/blocks/oreRedstone.png");
            allow.add("textures/blocks/clay.png");
            allow.add("textures/blocks/netherBrick.png");
            allow.add("textures/blocks/oreIron.png");
            allow.add("textures/blocks/oreEmerald.png");
            allow.add("textures/blocks/lava.png");

            allow.add("textures/blocks/lazurite_ore.png");
            allow.add("textures/blocks/pyrite_ore.png");
            allow.add("textures/blocks/scandium_ore.png");
        }
    }

    private static BufferedImage readTextureImage(InputStream par1InputStream) throws IOException
    {
        BufferedImage var2 = ImageIO.read(par1InputStream);
        par1InputStream.close();
        return var2;
    }

    public static boolean checkTexturePack()
    {
        IResourceManager par1ResourceManager = Minecraft.getMinecraft().getResourceManager();
        init();

        for (int i = 0; i < allow.size(); ++i)
        {
            String filename = (String)allow.get(i);

            try
            {
                IResource e = par1ResourceManager.getResource(new ResourceLocation(filename));
                InputStream var7 = e.getInputStream();
                BufferedImage var9 = ImageIO.read(var7);
                int pixnb = countTransparentPixels(var9);

                if (pixnb > 0)
                {
                    return false;
                }
            }
            catch (IOException var71)
            {
            }
        }

        return true;
    }

    private static int countTransparentPixels(BufferedImage par1BufferedImage)
    {
        int pixnb = 0;
        int w = par1BufferedImage.getWidth();
        int h = par1BufferedImage.getHeight();

        for (int x = 0; x < w; ++x)
        {
            for (int y = 0; y < h; ++y)
            {
                int rgb = par1BufferedImage.getRGB(x, y);
                int alpha = rgb >> 24 & 255;

                if (alpha != 255)
                {
                    ++pixnb;
                }
            }
        }

        return pixnb;
    }
}
