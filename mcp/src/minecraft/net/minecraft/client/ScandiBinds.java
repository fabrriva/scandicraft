package net.minecraft.client;

import java.util.ArrayList;

import net.minecraft.client.Minecraft;
import net.minecraft.client.settings.KeyBinding;

import org.lwjgl.input.Keyboard;

public enum ScandiBinds {
	
	armor(new KeyBinding("key.armorgui", Keyboard.KEY_F12, "key.categories.scandicraft")),
	potion(new KeyBinding("key.potion", Keyboard.KEY_P, "key.categories.scandicraft")),
	types(new KeyBinding("key.tp", 0, "key.categories.scandicraft")),
	fhome(new KeyBinding("key.fhome", 0, "key.categories.scandicraft")),
	home(new KeyBinding("key.home", 0, "key.categories.scandicraft")),
	feed(new KeyBinding("key.feed", 0, "key.categories.scandicraft")),
	cback(new KeyBinding("key.cback", 0, "key.categories.scandicraft"));
	
	private KeyBinding keyb;
	
	ScandiBinds(KeyBinding kb)
	{
		this.keyb=kb;
	}
	
	
	public KeyBinding getKeyBinding()
	{
		return this.keyb;
	}
	
	public static void init(Minecraft mc)
	{
        ArrayList<KeyBinding> temp = new ArrayList<KeyBinding>();
        for(int x=0;x<mc.gameSettings.keyBindings.length;x++)
        {
        	temp.add(mc.gameSettings.keyBindings[x]);
        }
        
        for(ScandiBinds qb:ScandiBinds.values())
        {
        	temp.add(qb.getKeyBinding());
        }
        
        for(KeyBinding kb:temp)
        {
        	for(KeyBinding tested:temp)
        	{
        		if(kb.getKeyCode()==tested.getKeyCode())
        		{
        		}
        	}
        }
        
        mc.gameSettings.keyBindings= new KeyBinding[temp.size()];
        int x = 0;
        for(KeyBinding kb:temp)
        {
        	mc.gameSettings.keyBindings[x]=kb;
        	x++;
        }
	}
}