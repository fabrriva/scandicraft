package net.minecraft.block;

import java.util.Random;
import net.minecraft.init.Items;
import net.minecraft.item.Item;

public class BlockScandiumOre extends BlockOre
{
    public Item getItemDropped(int paramInt1, Random paramRandom, int paramInt2)
    {
    	Item itemDrop;
    	
    	if (paramRandom.nextInt(2) == 0) {
    		itemDrop = Items.scandium;
		} else {
			itemDrop = Items.scandium_nugget;
		}
    	
        return itemDrop;
    }
    
    public int quantityDropped()
    {
        return 1;
    }
}
