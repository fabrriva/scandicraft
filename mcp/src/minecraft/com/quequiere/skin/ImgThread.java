package com.quequiere.skin;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;

import javax.imageio.ImageIO;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;

public class ImgThread extends Thread {
	

	
	public CustomSkin skin;
	
	public ImgThread(CustomSkin skin) {
		this.skin = skin;
	}

	public void run() 
	{
		
		System.out.println("Download "+this.skin.getPseudo()+" skin...");

		try {
			URL url = new URL("http://193.70.6.43/webroot/gestion/skins/"+this.skin.getPseudo()+".png");
			URLConnection con = url.openConnection();
			InputStream inputStream = con.getInputStream();
			Image image = ImageIO.read(inputStream);
			BufferedImage img = (BufferedImage) image;
			
			this.skin.setBuffer(img);
			
			
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		System.out.println("Download "+this.skin.getPseudo()+"skin successful.");

	}
	
}